package entity

type GetSingleProductResponse struct {
	// PlatformID is string(10)
	//  平台商或POS商會員編號[MerchantID]
	PlatformID string `json:"PlatformID"`
	// MerchantID is string(10)
	//  特店編號[必]
	MerchantID string `json:"MerchantID"`
	// RtnCode is int
	//  回應代碼
	//  1:成功, 其他:失敗
	RtnCode int `json:"RtnCode"`
	// RtnMsg is string(100)
	//  回應訊息
	RtnMsg string `json:"RtnMsg"`
	// ItemNo is string(8)
	//  商品編號
	ItemNo string `json:"ItemNo"`
	// ItemName is string(20)
	//  商品名稱
	ItemName string `json:"ItemName"`
	// TicketType is string(1)
	//  票券類別
	//  1:提貨券, 2:贈品券
	TicketType string `json:"TicketType"`
	// ItemAmount is int
	//  商品面額
	ItemAmount int `json:"ItemAmount"`
	// ItemStatus is string(1)
	//  商品狀態
	//  0:已下架, 1:上架中
	ItemStatus string `json:"ItemStatus"`
	// ModifyDate is string(14)
	//  最後異動時間
	//  此商品最後一次異動的時間，格式為：yyyymmdd hh:mm
	ModifyDate string `json:"ModifyDate"`
	// InstructionsList is an array
	//  票券使用說明清單
	InstructionsList []Instruction `json:"InstructionsList"`
}

type Instruction struct {
	// IssueType is string(1)
	//  出券類型
	//  1：超商票券
	//  2：紙本票券
	//  3：電子票券
	IssueType string `json:"IssueType"`
	// Description is string(100)
	//  使用說明
	//  依出券類型[IssueType]回傳該類型的票券使用說明事項
	Description string `json:"Description"`
}
