package entity

type WriteOffRequest struct {
	// PlatformID is string(10)
	//  平台商或POS商會員編號[MerchantID]
	PlatformID string
	// MerchantID is string(10)
	//  特店編號[必]
	MerchantID string
	// HashKey is string
	//  密鑰[必]
	HashKey string
	// HashIV is string
	//  iv[必]
	HashIV string
	WriteOffData
}

type WriteOffData struct {
	// WriteOffNo is string(18)
	//  核銷代碼[必]
	//  每張票券會有一組專屬核銷代碼，掃描消費者出示的票券Barcode取得
	WriteOffNo string `json:"WriteOffNo"`
	// Action is string(1)
	//  執行動作[必]
	//  1：核銷
	//  2：取消核銷
	// 注意：
	//  贈品券不可取消核銷
	Action string `json:"Action"`
	// CancelReason is string(50)
	//  取消核銷原因
	//  執行動作為取消核銷時(Action=2)，可帶入取消核銷原因
	CancelReason string `json:"CancelReason"`
	// StoreID is string(20)
	//  分店編號
	//  執行此筆票券核銷/取消核銷的分店。
	//  當有傳入此參數時，系統會檢核分店是否存在且狀態為啟用中，
	//  並判斷是否具備核銷/取消核銷權限，如不需權限控管請勿填寫。
	// 注意：
	//  請先於綠界廠商後台建立分店資料，再傳入此分店編號。
	//  ※如何建立分店資料？請參考：票券發行管理平台操作手冊 (Page.35～Page.43)
	//  限英、數字，且不可使用全形
	//  長度限制為 2~20 個字
	//  當欄位為空值時，代表此筆為總店發行。
	StoreID string `json:"StoreID"`
	// Operator is string(10)
	//  建立人員[必]
	//  操作此筆票券核銷/取消核銷人員，僅為歷程紀錄使用
	// 注意：
	//  限英、數字，且不可使用全形
	Operator string `json:"Operator"`
}

type writeOffHandler func(request *WriteOffRequest)

type WriteOffRequestBuilder struct {
	actions []writeOffHandler
}

// WithPlatformID is string(10)
//  平台商或POS商會員編號[MerchantID]
func (b *WriteOffRequestBuilder) WithPlatformID(id string) *WriteOffRequestBuilder {
	b.actions = append(b.actions, func(request *WriteOffRequest) {
		request.PlatformID = id
	})
	return b
}

// WithMerchantID is string(10)
//  特店編號[必]
func (b *WriteOffRequestBuilder) WithMerchantID(id string) *WriteOffRequestBuilder {
	b.actions = append(b.actions, func(request *WriteOffRequest) {
		request.MerchantID = id
	})
	return b
}

// HasHashKey is string
//  密鑰[必]
func (b *WriteOffRequestBuilder) HasHashKey(key string) *WriteOffRequestBuilder {
	b.actions = append(b.actions, func(request *WriteOffRequest) {
		request.HashKey = key
	})
	return b
}

// HasHashIV is string
//  iv[必]
func (b *WriteOffRequestBuilder) HasHashIV(iv string) *WriteOffRequestBuilder {
	b.actions = append(b.actions, func(request *WriteOffRequest) {
		request.HashIV = iv
	})
	return b
}

// WithWriteOffNo is string(18)
//  核銷代碼[必]
//  每張票券會有一組專屬核銷代碼，掃描消費者出示的票券Barcode取得
func (b *WriteOffRequestBuilder) WithWriteOffNo(no string) *WriteOffRequestBuilder {
	b.actions = append(b.actions, func(data *WriteOffRequest) {
		data.WriteOffNo = no
	})
	return b
}

// Action is string(1)
//  執行動作[必]
//  1：核銷
//  2：取消核銷
// 注意：
//  贈品券不可取消核銷
func (b *WriteOffRequestBuilder) Action(action string) *WriteOffRequestBuilder {
	b.actions = append(b.actions, func(data *WriteOffRequest) {
		data.Action = action
	})
	return b
}

// HasCancelReason is string(50)
//  取消核銷原因
//  執行動作為取消核銷時(Action=2)，可帶入取消核銷原因
func (b *WriteOffRequestBuilder) HasCancelReason(reason string) *WriteOffRequestBuilder {
	b.actions = append(b.actions, func(data *WriteOffRequest) {
		data.CancelReason = reason
	})
	return b
}

// WriteOffByStore is string(20)
//  分店編號
//  執行此筆票券核銷/取消核銷的分店。
//  當有傳入此參數時，系統會檢核分店是否存在且狀態為啟用中，
//  並判斷是否具備核銷/取消核銷權限，如不需權限控管請勿填寫。
// 注意：
//  請先於綠界廠商後台建立分店資料，再傳入此分店編號。
//  ※如何建立分店資料？請參考：票券發行管理平台操作手冊 (Page.35～Page.43)
//  限英、數字，且不可使用全形
//  長度限制為 2~20 個字
//  當欄位為空值時，代表此筆為總店發行。
func (b *WriteOffRequestBuilder) WriteOffByStore(id string) *WriteOffRequestBuilder {
	b.actions = append(b.actions, func(data *WriteOffRequest) {
		data.StoreID = id
	})
	return b
}

// OperateBy is string(10)
//  建立人員[必]
//  操作此筆票券核銷/取消核銷人員，僅為歷程紀錄使用
// 注意：
//  限英、數字，且不可使用全形
func (b *WriteOffRequestBuilder) OperateBy(operator string) *WriteOffRequestBuilder {
	b.actions = append(b.actions, func(data *WriteOffRequest) {
		data.Operator = operator
	})
	return b
}

// Build builds the request object
func (b *WriteOffRequestBuilder) Build() WriteOffRequest {
	data := WriteOffRequest{}
	for _, action := range b.actions {
		action(&data)
	}
	return data
}

// NewWriteOffRequestBuilder - constructor
func NewWriteOffRequestBuilder() *WriteOffRequestBuilder {
	return &WriteOffRequestBuilder{}
}
