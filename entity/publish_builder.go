package entity

type PublishRequest struct {
	// PlatformID is string(10)
	//  平台商或POS商會員編號[MerchantID]
	PlatformID string
	// MerchantID is string(10)
	//  特店編號[必]
	MerchantID string
	// HashKey is string
	//  密鑰[必]
	HashKey string
	// HashIV is string
	//  iv[必]
	HashIV string
	PublishData
	Customer
}

type Customer struct {
	// Name is string(20)
	//  購買人姓名
	// 注意:
	//  當IssueType=3(電子票券)時，此欄位為必填
	//  當IssueType=2(紙本票券)時，若特店有寄送紙本票券給購買人的需求時，此欄位請務必填寫。
	//  限純中文或純英文，中英文不可混用。
	//  當欄位有填寫資料時，至少需帶入2個字元以上
	Name string
	// Phone is string(10)
	//  購買人手機
	// 注意:
	//  當IssueType=3(電子票券)時，此欄位為必填
	//  限數字、須為09開頭
	//  僅接受國內手機號碼
	Phone string
	// Email is string(80)
	//  購買人電子信箱
	// 注意:
	//  當IssueType=1(超商票券)或3(電子票券)時，此欄位為必填
	Email string
	// Address is string(100)
	//  購買人地址
	// 注意:
	//  當IssueType=2(紙本票券)時，若特店有寄送紙本票券給購買人的需求時，此欄位請務必填寫。
	Address string
}

type PublishData struct {
	// MerchantTradeNo is string(25)
	//  特店訂單編號
	// 注意:
	//  若訂單包含提貨券時，此欄位為必填；若訂單皆為贈品券時，則此欄位必須為空不可填寫。
	//  特店訂單編號應為唯一值，不可重覆
	//  限英、數字
	MerchantTradeNo string
	// FreeTradeNo is string(20)
	//  贈品單號
	// 注意:
	//  若訂單皆為贈品券時(MerchantTradeNo=空)，此欄位為必填；若訂單包含提貨券時，則此欄位必須為空不可填寫。
	//  贈品單號應為唯一值，不可重覆
	//  限英、數字
	FreeTradeNo string
	// IssueType is string(1)
	//  出券類型[必]
	//  1：超商票券
	//  2：紙本票券
	//  3：電子票券
	IssueType string
	// PrintType is string(1)
	//  列印類型
	//  1：綠界列印
	//  2：廠商列印
	// 注意:
	//  當 IssueType=2(紙本票券) 時，此欄位為必填
	PrintType string
	// IsImmediate is string(1)
	//  是否立即使用
	//  1：即期，不須設定電子票券生效日[StartDate]，票券發行當下消費者可立即使用。
	//  2：非即期，須設定電子票券生效日[StartDate]，生效日最早可設定發行日隔一天，常用於預售票情境。
	// 注意:
	//  當IssueType=3(電子票券)時，此欄位為必填
	//  超商及紙本票券僅能以即期方式發行，故當IssueType=1(超商票券)或2(紙本票券)時，不需填寫此欄位，系統固定設定為即期。
	IsImmediate string
	// RefundNotifyURL is string(200)
	//  退款結果主動通知URL
	//  當提貨券訂單發生退款時，ECTicket Server將主動以Server POST方式通知平台商或特店退款相關資訊。
	// 注意:
	//  URL Domain需事先與綠界申請開通防火牆
	//  若平台商需收到退款通知且訂單包含提貨券時，請填寫此參數才可收到通知。
	RefundNotifyURL string
	// StoreID is string(20)
	//  分店編號
	//  執行此筆票券發行的分店。當有傳入此參數時，系統會檢核分店是否存在且狀態為啟用中。
	// 注意:
	//  請先於綠界廠商後台建立分店資料，再傳入此分店編號。
	//  ※如何建立分店資料？請參考：票券發行管理平台操作手冊 (Page.35～Page.43)
	//  限英、數字，且不可使用全形
	//  長度限制為 2~20 個字
	//  當欄位為空值時，代表此筆為總店發行。
	StoreID string
	// Operator is string(10)
	//  建立人員[必]
	//  建立此筆票券發行人員，僅為歷程紀錄使用
	// 注意:
	//  限英、數字，且不可使用全形
	Operator string
	// TicketInfo is Ticket[]
	//  票券資料
	TicketInfo []Ticket
}

type Ticket struct {
	// ItemNo is string(8)
	//  商品編號[必]
	// 注意:
	//  限英、數字
	//  單筆訂單，商品編號不可重複
	ItemNo string `json:"ItemNo"`
	// TicketAmount is int
	//  票券發行張數[必]
	// 注意:
	//  當IssueType=1(超商票券)時，單筆訂單加總限制最多發行8張
	//  當IssueType=2(紙本票券)或3(電子票券)時，單筆訂單加總限制最多發行100張
	TicketAmount int `json:"TicketAmount"`
	// StartDate is string(8)
	//  票券生效日
	//  票券生效日即票券可開始使用日期，格式為：yyyymmdd
	// 注意:
	//  當IsImmediate=1 (即期)時，此欄位固定為票券發行當天的日期
	//  IsImmediate=2 (非即期)時，此欄位為必填，須大於票券發行當天的日期
	StartDate string `json:"StartDate"`
	// ExpireDate is string(8)
	//  贈品券到期日期
	//  格式為：yyyymmdd
	// 注意:
	//  當商品為贈品券時，此欄位才需填寫且為必填
	//  日期不可小於等於票券生效日(StartDate)
	ExpireDate string `json:"ExpireDate"`
}

type ticketHandler func(t *Ticket)

type TicketBuilder struct {
	actions []ticketHandler
}

// WithItemNo is string(8)
//  商品編號[必]
// 注意:
//  限英、數字
//  單筆訂單，商品編號不可重複
func (t *TicketBuilder) WithItemNo(number string) *TicketBuilder {
	t.actions = append(t.actions, func(t *Ticket) {
		t.ItemNo = number
	})
	return t
}

// Has sets ticket amount
//  票券發行張數[必]
// 注意:
//  當IssueType=1(超商票券)時，單筆訂單加總限制最多發行8張
//  當IssueType=2(紙本票券)或3(電子票券)時，單筆訂單加總限制最多發行100張
func (t *TicketBuilder) Has(amount int) *TicketBuilder {
	t.actions = append(t.actions, func(t *Ticket) {
		t.TicketAmount = amount
	})
	return t
}

// StartFrom is string(8)
//  票券生效日
//  票券生效日即票券可開始使用日期，格式為：yyyymmdd
// 注意:
//  當IsImmediate=1 (即期)時，此欄位固定為票券發行當天的日期
//  IsImmediate=2 (非即期)時，此欄位為必填，須大於票券發行當天的日期
func (t *TicketBuilder) StartFrom(date string) *TicketBuilder {
	t.actions = append(t.actions, func(t *Ticket) {
		t.StartDate = date
	})
	return t
}

// ExpireAt is string(8)
//  贈品券到期日期
//  格式為：yyyymmdd
// 注意:
//  當商品為贈品券時，此欄位才需填寫且為必填
//  日期不可小於等於票券生效日(startDate)
func (t *TicketBuilder) ExpireAt(date string) *TicketBuilder {
	t.actions = append(t.actions, func(t *Ticket) {
		t.ExpireDate = date
	})
	return t
}

// Build builds the ticket object
func (t *TicketBuilder) Build() *Ticket {
	req := &Ticket{}
	for _, action := range t.actions {
		action(req)
	}
	return req
}

// NewTicketBuilder - constructor
func NewTicketBuilder() *TicketBuilder {
	return &TicketBuilder{}
}

type publishHandler func(req *PublishRequest)

type PublishRequestBuilder struct {
	actions []publishHandler
}

// WithPlatformID is string(10)
//  平台商或POS商會員編號[MerchantID]
func (rb *PublishRequestBuilder) WithPlatformID(id string) *PublishRequestBuilder {
	rb.actions = append(rb.actions, func(req *PublishRequest) {
		req.PlatformID = id
	})
	return rb
}

// WithMerchantID is string(10)
//  特店編號[必]
func (rb *PublishRequestBuilder) WithMerchantID(id string) *PublishRequestBuilder {
	rb.actions = append(rb.actions, func(req *PublishRequest) {
		req.MerchantID = id
	})
	return rb
}

// HasHashKey is string
//  密鑰[必]
func (rb *PublishRequestBuilder) HasHashKey(key string) *PublishRequestBuilder {
	rb.actions = append(rb.actions, func(req *PublishRequest) {
		req.HashKey = key
	})
	return rb
}

// HasHashIV is string
//  iv[必]
func (rb *PublishRequestBuilder) HasHashIV(iv string) *PublishRequestBuilder {
	rb.actions = append(rb.actions, func(req *PublishRequest) {
		req.HashIV = iv
	})
	return rb
}

// WithCustomerName is string(20)
//  購買人姓名
// 注意:
//  當IssueType=3(電子票券)時，此欄位為必填
//  當IssueType=2(紙本票券)時，若特店有寄送紙本票券給購買人的需求時，此欄位請務必填寫。
//  限純中文或純英文，中英文不可混用。
//  當欄位有填寫資料時，至少需帶入2個字元以上
func (rb *PublishRequestBuilder) WithCustomerName(name string) *PublishRequestBuilder {
	rb.actions = append(rb.actions, func(req *PublishRequest) {
		req.Name = name
	})
	return rb
}

// WithCustomerPhone is string(10)
//  購買人手機
// 注意:
//  當IssueType=3(電子票券)時，此欄位為必填
//  限數字、須為09開頭
//  僅接受國內手機號碼
func (rb *PublishRequestBuilder) WithCustomerPhone(phone string) *PublishRequestBuilder {
	rb.actions = append(rb.actions, func(req *PublishRequest) {
		req.Phone = phone
	})
	return rb
}

// WithCustomerEmail is string(80)
//  購買人電子信箱
// 注意:
//  當IssueType=1(超商票券)或3(電子票券)時，此欄位為必填
func (rb *PublishRequestBuilder) WithCustomerEmail(email string) *PublishRequestBuilder {
	rb.actions = append(rb.actions, func(req *PublishRequest) {
		req.Email = email
	})
	return rb
}

// WithCustomerAddress is string(100)
//  購買人地址
// 注意:
//  當IssueType=2(紙本票券)時，若特店有寄送紙本票券給購買人的需求時，此欄位請務必填寫。
func (rb *PublishRequestBuilder) WithCustomerAddress(address string) *PublishRequestBuilder {
	rb.actions = append(rb.actions, func(req *PublishRequest) {
		req.Address = address
	})
	return rb
}

// WithMerchantTradeNo is string(25)
//  特店訂單編號
// 注意:
//  若訂單包含提貨券時，此欄位為必填；若訂單皆為贈品券時，則此欄位必須為空不可填寫。
//  特店訂單編號應為唯一值，不可重覆
//  限英、數字
func (rb *PublishRequestBuilder) WithMerchantTradeNo(number string) *PublishRequestBuilder {
	rb.actions = append(rb.actions, func(req *PublishRequest) {
		req.MerchantTradeNo = number
	})
	return rb
}

// WithFreeTradeNo is string(20)
//  贈品單號
// 注意:
//  若訂單皆為贈品券時(MerchantTradeNo=空)，此欄位為必填；若訂單包含提貨券時，則此欄位必須為空不可填寫。
//  贈品單號應為唯一值，不可重覆
//  限英、數字
func (rb *PublishRequestBuilder) WithFreeTradeNo(number string) *PublishRequestBuilder {
	rb.actions = append(rb.actions, func(req *PublishRequest) {
		req.FreeTradeNo = number
	})
	return rb
}

// WithIssueType is string(1)
//  出券類型[必]
//  1：超商票券
//  2：紙本票券
//  3：電子票券
func (rb *PublishRequestBuilder) WithIssueType(t string) *PublishRequestBuilder {
	rb.actions = append(rb.actions, func(req *PublishRequest) {
		req.IssueType = t
	})
	return rb
}

// WithPrintType is string(1)
//  列印類型
//  1：綠界列印
//  2：廠商列印
// 注意:
//  當 IssueType=2(紙本票券) 時，此欄位為必填
func (rb *PublishRequestBuilder) WithPrintType(t string) *PublishRequestBuilder {
	rb.actions = append(rb.actions, func(req *PublishRequest) {
		req.PrintType = t
	})
	return rb
}

// WithRefundNotifyURL is string(200)
//  退款結果主動通知URL
//  當提貨券訂單發生退款時，ECTicket Server將主動以Server POST方式通知平台商或特店退款相關資訊。
// 注意:
//  URL Domain需事先與綠界申請開通防火牆
//  若平台商需收到退款通知且訂單包含提貨券時，請填寫此參數才可收到通知。
func (rb *PublishRequestBuilder) WithRefundNotifyURL(url string) *PublishRequestBuilder {
	rb.actions = append(rb.actions, func(req *PublishRequest) {
		req.RefundNotifyURL = url
	})
	return rb
}

// WithIsImmediate is string(1)
//  是否立即使用
//  1：即期，不須設定電子票券生效日[StartDate]，票券發行當下消費者可立即使用。
//  2：非即期，須設定電子票券生效日[StartDate]，生效日最早可設定發行日隔一天，常用於預售票情境。
// 注意:
//  當IssueType=3(電子票券)時，此欄位為必填
//  超商及紙本票券僅能以即期方式發行，故當IssueType=1(超商票券)或2(紙本票券)時，不需填寫此欄位，系統固定設定為即期。
func (rb *PublishRequestBuilder) WithIsImmediate(i string) *PublishRequestBuilder {
	rb.actions = append(rb.actions, func(req *PublishRequest) {
		req.IsImmediate = i
	})
	return rb
}

// WithStoreID is string(20)
//  分店編號
//  執行此筆票券發行的分店。當有傳入此參數時，系統會檢核分店是否存在且狀態為啟用中。
// 注意:
//  請先於綠界廠商後台建立分店資料，再傳入此分店編號。
//  ※如何建立分店資料？請參考：票券發行管理平台操作手冊 (Page.35～Page.43)
//  限英、數字，且不可使用全形
//  長度限制為 2~20 個字
//  當欄位為空值時，代表此筆為總店發行。
func (rb *PublishRequestBuilder) WithStoreID(id string) *PublishRequestBuilder {
	rb.actions = append(rb.actions, func(req *PublishRequest) {
		req.StoreID = id
	})
	return rb
}

// OperateBy is string(10)
//  建立人員[必]
//  建立此筆票券發行人員，僅為歷程紀錄使用
// 注意:
//  限英、數字，且不可使用全形
func (rb *PublishRequestBuilder) OperateBy(op string) *PublishRequestBuilder {
	rb.actions = append(rb.actions, func(req *PublishRequest) {
		req.Operator = op
	})
	return rb
}

// HasSingle is to add a ticket to request
func (rb *PublishRequestBuilder) HasSingle(ticket *Ticket) *PublishRequestBuilder {
	rb.actions = append(rb.actions, func(req *PublishRequest) {
		req.TicketInfo = append(req.TicketInfo, *ticket)
	})
	return rb
}

// HasMultiple is to add a list of ticket to request
func (rb *PublishRequestBuilder) HasMultiple(tickets []*Ticket) *PublishRequestBuilder {
	t := make([]Ticket, len(tickets))
	for i, ticket := range tickets {
		t[i] = *ticket
	}
	rb.actions = append(rb.actions, func(req *PublishRequest) {
		req.TicketInfo = append(req.TicketInfo, t...)
	})
	return rb
}

// Build builds the request object
func (rb *PublishRequestBuilder) Build() PublishRequest {
	req := PublishRequest{}
	for _, action := range rb.actions {
		action(&req)
	}
	return req
}

// NewRequestBuilder - constructor
func NewRequestBuilder() *PublishRequestBuilder {
	return &PublishRequestBuilder{}
}
