package entity

type WriteOffResponse struct {
	// PlatformID is string(10)
	//  平台商或POS商會員編號[MerchantID]
	PlatformID string `json:"PlatformID"`
	// MerchantID is string(10)
	//  特店編號[必]
	MerchantID string `json:"MerchantID"`
	// RtnCode is int
	//  回應代碼
	//  1:成功, 其他:失敗
	RtnCode int `json:"RtnCode"`
	// RtnMsg is string(100)
	//  回應訊息
	RtnMsg string `json:"RtnMsg"`
}
