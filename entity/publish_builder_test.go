package entity_test

import (
	"ec-ticket/entity"
	"fmt"
)

func ExamplePublishRequestBuilder_Build_Single() {
	req := entity.NewRequestBuilder().
		WithPlatformID("1231231234").
		WithMerchantID("4564564567").
		HasHashKey("abc").
		HasHashIV("def").
		WithCustomerName("Winnie").
		WithCustomerPhone("0912345678").
		WithCustomerEmail("abc@gmail.com").
		WithCustomerAddress("Taipei").
		WithMerchantTradeNo("AAA123").
		WithFreeTradeNo("BBB456").
		WithIssueType("3").
		WithPrintType("1").
		WithRefundNotifyURL("https://www.winnie.com").
		WithIsImmediate("1").
		OperateBy("Winnie").
		HasSingle(entity.NewTicketBuilder().WithItemNo("A0001").Has(2).StartFrom("20231012").Build()).
		Build()
	fmt.Println(req.PlatformID)
	fmt.Println(req.MerchantID)
	fmt.Println(req.HashKey)
	fmt.Println(req.HashIV)
	fmt.Println(req.Customer.Name)
	fmt.Println(req.Customer.Phone)
	fmt.Println(req.Customer.Email)
	fmt.Println(req.Customer.Address)
	fmt.Println(req.MerchantTradeNo)
	fmt.Println(req.FreeTradeNo)
	fmt.Println(req.IssueType)
	fmt.Println(req.PrintType)
	fmt.Println(req.RefundNotifyURL)
	fmt.Println(req.IsImmediate)
	fmt.Println(req.Operator)
	fmt.Println(len(req.PublishData.TicketInfo))
	fmt.Println(req.PublishData.TicketInfo[0].ItemNo)
	fmt.Println(req.PublishData.TicketInfo[0].TicketAmount)

	// Output:
	// 1231231234
	// 4564564567
	// abc
	// def
	// Winnie
	// 0912345678
	// abc@gmail.com
	// Taipei
	// AAA123
	// BBB456
	// 3
	// 1
	// https://www.winnie.com
	// 1
	// Winnie
	// 1
	// A0001
	// 2
}

func ExamplePublishRequestBuilder_Build_Multiple() {
	ticket1 := entity.NewTicketBuilder().WithItemNo("A0001").Has(2).StartFrom("20231012").Build()
	ticket2 := entity.NewTicketBuilder().WithItemNo("A0002").Has(3).StartFrom("20231012").Build()
	req := entity.NewRequestBuilder().
		WithPlatformID("1231231234").
		WithMerchantID("4564564567").
		HasHashKey("abc").
		HasHashIV("def").
		WithCustomerName("Winnie").
		WithCustomerPhone("0912345678").
		WithCustomerEmail("abc@gmail.com").
		WithCustomerAddress("Taipei").
		WithMerchantTradeNo("AAA123").
		WithFreeTradeNo("BBB456").
		WithIssueType("3").
		WithPrintType("1").
		WithRefundNotifyURL("https://www.winnie.com").
		WithIsImmediate("1").
		OperateBy("Winnie").
		HasMultiple([]*entity.Ticket{ticket1, ticket2}).
		Build()
	fmt.Println(req.PlatformID)
	fmt.Println(req.MerchantID)
	fmt.Println(req.HashKey)
	fmt.Println(req.HashIV)
	fmt.Println(req.Customer.Name)
	fmt.Println(req.Customer.Phone)
	fmt.Println(req.Customer.Email)
	fmt.Println(req.Customer.Address)
	fmt.Println(req.MerchantTradeNo)
	fmt.Println(req.FreeTradeNo)
	fmt.Println(req.IssueType)
	fmt.Println(req.PrintType)
	fmt.Println(req.RefundNotifyURL)
	fmt.Println(req.IsImmediate)
	fmt.Println(req.Operator)
	fmt.Println(len(req.PublishData.TicketInfo))
	fmt.Println(req.PublishData.TicketInfo[0].ItemNo)
	fmt.Println(req.PublishData.TicketInfo[0].TicketAmount)
	fmt.Println(req.PublishData.TicketInfo[1].ItemNo)
	fmt.Println(req.PublishData.TicketInfo[1].TicketAmount)

	// Output:
	// 1231231234
	// 4564564567
	// abc
	// def
	// Winnie
	// 0912345678
	// abc@gmail.com
	// Taipei
	// AAA123
	// BBB456
	// 3
	// 1
	// https://www.winnie.com
	// 1
	// Winnie
	// 2
	// A0001
	// 2
	// A0002
	// 3
}
