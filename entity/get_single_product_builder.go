package entity

type GetSingleProductRequest struct {
	PlatformID string `json:"PlatformID"`
	MerchantID string `json:"MerchantID"`
	HashKey    string `json:"HashKey"`
	HashIV     string `json:"HashIV"`
	ItemNo     string `json:"ItemNo"`
}

type getSingleProductHandler func(req *GetSingleProductRequest)

type GetSingleProductRequestBuilder struct {
	actions []getSingleProductHandler
}

// WithPlatformID is string(10)
//  平台商或POS商會員編號[MerchantID]
func (gb *GetSingleProductRequestBuilder) WithPlatformID(id string) *GetSingleProductRequestBuilder {
	gb.actions = append(gb.actions, func(req *GetSingleProductRequest) {
		req.PlatformID = id
	})
	return gb
}

// WithMerchantID is string(10)
//  特店編號[必]
func (gb *GetSingleProductRequestBuilder) WithMerchantID(id string) *GetSingleProductRequestBuilder {
	gb.actions = append(gb.actions, func(req *GetSingleProductRequest) {
		req.MerchantID = id
	})
	return gb
}

// HasHashKey is string
//  密鑰[必]
func (gb *GetSingleProductRequestBuilder) HasHashKey(key string) *GetSingleProductRequestBuilder {
	gb.actions = append(gb.actions, func(req *GetSingleProductRequest) {
		req.HashKey = key
	})
	return gb
}

// HasHashIV is string
//  iv[必]
func (gb *GetSingleProductRequestBuilder) HasHashIV(iv string) *GetSingleProductRequestBuilder {
	gb.actions = append(gb.actions, func(req *GetSingleProductRequest) {
		req.HashIV = iv
	})
	return gb
}

// WithItemNo is string(8)
//  商品編號[必]
func (gb *GetSingleProductRequestBuilder) WithItemNo(itemNo string) *GetSingleProductRequestBuilder {
	gb.actions = append(gb.actions, func(req *GetSingleProductRequest) {
		req.ItemNo = itemNo
	})
	return gb
}

// Build builds the request object
func (gb *GetSingleProductRequestBuilder) Build() GetSingleProductRequest {
	req := GetSingleProductRequest{}
	for _, action := range gb.actions {
		action(&req)
	}
	return req
}

// NewGetSingleProductRequestBuilder - constructor
func NewGetSingleProductRequestBuilder() *GetSingleProductRequestBuilder {
	return &GetSingleProductRequestBuilder{}
}
