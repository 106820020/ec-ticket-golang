package entity

type PublishResponse struct {
	// PlatformID is string(10)
	//  平台商或POS商會員編號[MerchantID]
	PlatformID string `json:"PlatformID"`
	// MerchantID is string(10)
	//  特店編號[必]
	MerchantID string `json:"MerchantID"`
	// RtnCode is int
	//  回應代碼
	//  1:成功, 其他:失敗
	RtnCode int `json:"RtnCode"`
	// RtnMsg is string(100)
	//  回應訊息
	RtnMsg string `json:"RtnMsg"`
	// MerchantTradNo is string(25)
	//  特店訂單編號
	MerchantTradNo string `json:"MerchantTradNo"`
	// FreeTradeNo is string(20)
	//  贈品單號
	FreeTradeNo string `json:"FreeTradeNo"`
	// TicketTradeNo is string(16)
	//  票券訂單編號
	TicketTradeNo string `json:"TicketTradeNo"`
	// TicketList is an array
	//  票券資料
	TicketList []TicketData `json:"TicketData"`
}

type TicketData struct {
	// ItemNo is string(8)
	//  商品編號
	ItemNo string `json:"ItemNo"`
	// TicketAmount is int
	//  票券發行張數
	TicketAmount int `json:"TicketAmount"`
}
