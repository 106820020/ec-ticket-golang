package command

import (
	"bytes"
	"ec-ticket/entity"
	"ec-ticket/usecase"
	"encoding/json"
	"net/http"
	"time"
)

type PublishTicketUseCaseReq struct {
	requestData *entity.PublishRequest
	url         string
}

type PublishTicketUseCaseRes struct {
	ResponseData *entity.PublishResponse
	Err          error
}

type PublishTicketUseCase struct {
	req *PublishTicketUseCaseReq
	res *PublishTicketUseCaseRes
}

type publishData struct {
	MerchantID      string          `json:"MerchantID"`
	MerchantTradeNo string          `json:"MerchantTradeNo"`
	FreeTradeNo     string          `json:"FreeTradeNo"`
	IssueType       string          `json:"IssueType"`
	PrintType       string          `json:"PrintType"`
	IsImmediate     string          `json:"IsImmediate"`
	RefundNotifyURL string          `json:"RefundNotifyURL"`
	StoreID         string          `json:"StoreID"`
	Operator        string          `json:"Operator"`
	CustomerName    string          `json:"CustomerName"`
	CustomerPhone   string          `json:"CustomerPhone"`
	CustomerEmail   string          `json:"CustomerEmail"`
	CustomerAddress string          `json:"CustomerAddress"`
	TicketInfo      []entity.Ticket `json:"TicketInfo"`
}

func (pc *PublishTicketUseCase) Execute() {
	// prepare data
	data := publishData{
		MerchantID:      pc.req.requestData.MerchantID,
		MerchantTradeNo: pc.req.requestData.MerchantTradeNo,
		FreeTradeNo:     pc.req.requestData.FreeTradeNo,
		IssueType:       pc.req.requestData.IssueType,
		PrintType:       pc.req.requestData.PrintType,
		IsImmediate:     pc.req.requestData.IsImmediate,
		RefundNotifyURL: pc.req.requestData.RefundNotifyURL,
		StoreID:         pc.req.requestData.StoreID,
		Operator:        pc.req.requestData.Operator,
		CustomerName:    pc.req.requestData.Customer.Name,
		CustomerPhone:   pc.req.requestData.Customer.Phone,
		CustomerEmail:   pc.req.requestData.Customer.Email,
		CustomerAddress: pc.req.requestData.Customer.Address,
		TicketInfo:      pc.req.requestData.TicketInfo,
	}
	jsonData, err := json.Marshal(data)
	if err != nil {
		pc.res.Err = err
		return
	}
	encrypt := usecase.EncryptData(string(jsonData), pc.req.requestData.HashKey, pc.req.requestData.HashIV)
	checkValueEncode := usecase.EncryptCheckMacValue(string(jsonData), pc.req.requestData.HashKey, pc.req.requestData.HashIV)

	param, err := json.Marshal(&usecase.CommonParam{
		PlatformID:    pc.req.requestData.PlatformID,
		MerchantID:    pc.req.requestData.MerchantID,
		RqHeader:      usecase.RpHeader{Timestamp: time.Now().Add(time.Minute * 10).Unix()},
		Data:          encrypt,
		CheckMacValue: checkValueEncode,
	})
	if err != nil {
		pc.res.Err = err
		return
	}

	// send request
	response, err := http.Post(pc.req.url, "application/json", bytes.NewBuffer(param))
	if err != nil {
		pc.res.Err = err
		return
	}
	defer response.Body.Close()
	tmp := &usecase.CommonResponse{}
	err = json.NewDecoder(response.Body).Decode(tmp)
	if err != nil {
		pc.res.Err = err
		return
	}
	decrypt := usecase.DecryptData(tmp.Data, pc.req.requestData.HashKey, pc.req.requestData.HashIV)
	res := &entity.PublishResponse{}
	err = json.Unmarshal([]byte(decrypt), res)
	if err != nil {
		pc.res.Err = err
		return
	}
	pc.res.ResponseData = res
}

func NewPublishTicketUseCase(
	req *PublishTicketUseCaseReq,
	res *PublishTicketUseCaseRes,
) usecase.UseCase {
	return &PublishTicketUseCase{req: req, res: res}
}

func NewPublishTicketUseCaseReq(
	req *entity.PublishRequest,
	url string,
) *PublishTicketUseCaseReq {
	return &PublishTicketUseCaseReq{requestData: req, url: url}
}

func NewPublishTicketUseCaseRes() *PublishTicketUseCaseRes {
	return &PublishTicketUseCaseRes{}
}
