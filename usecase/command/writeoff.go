package command

import (
	"bytes"
	"ec-ticket/entity"
	"ec-ticket/usecase"
	"encoding/json"
	"net/http"
	"time"
)

type WriteOffUseCaseReq struct {
	requestData *entity.WriteOffRequest
	url         string
}

type WriteOffUseCaseRes struct {
	ResponseData *entity.WriteOffResponse
	Err          error
}

type WriteOffUseCase struct {
	req *WriteOffUseCaseReq
	res *WriteOffUseCaseRes
}

type writeOffData struct {
	MerchantID string `json:"MerchantID"`
	entity.WriteOffData
}

func (wc *WriteOffUseCase) Execute() {
	// prepare data
	data := writeOffData{
		MerchantID: wc.req.requestData.MerchantID,
		WriteOffData: entity.WriteOffData{
			WriteOffNo:   wc.req.requestData.WriteOffNo,
			CancelReason: wc.req.requestData.CancelReason,
			Operator:     wc.req.requestData.Operator,
			Action:       wc.req.requestData.Action,
			StoreID:      wc.req.requestData.StoreID,
		},
	}
	jsonData, err := json.Marshal(data)
	if err != nil {
		wc.res.Err = err
		return
	}
	encrypt := usecase.EncryptData(string(jsonData), wc.req.requestData.HashKey, wc.req.requestData.HashIV)
	checkValueEncode := usecase.EncryptCheckMacValue(string(jsonData), wc.req.requestData.HashKey, wc.req.requestData.HashIV)

	param, err := json.Marshal(&usecase.CommonParam{
		PlatformID:    wc.req.requestData.PlatformID,
		MerchantID:    wc.req.requestData.MerchantID,
		RqHeader:      usecase.RpHeader{Timestamp: time.Now().Add(time.Minute * 10).Unix()},
		Data:          encrypt,
		CheckMacValue: checkValueEncode,
	})
	if err != nil {
		wc.res.Err = err
		return
	}

	// send request
	response, err := http.Post(wc.req.url, "application/json", bytes.NewBuffer(param))
	if err != nil {
		wc.res.Err = err
		return
	}
	defer response.Body.Close()
	tmp := &usecase.CommonResponse{}
	err = json.NewDecoder(response.Body).Decode(tmp)
	if err != nil {
		wc.res.Err = err
		return
	}
	decrypt := usecase.DecryptData(tmp.Data, wc.req.requestData.HashKey, wc.req.requestData.HashIV)
	res := &entity.WriteOffResponse{}
	err = json.Unmarshal([]byte(decrypt), res)
	if err != nil {
		wc.res.Err = err
		return
	}
	wc.res.ResponseData = res
}

func NewWriteOffUseCase(
	req *WriteOffUseCaseReq,
	res *WriteOffUseCaseRes,
) usecase.UseCase {
	return &WriteOffUseCase{req: req, res: res}
}

func NewWriteOffUseCaseReq(
	req *entity.WriteOffRequest,
	url string,
) *WriteOffUseCaseReq {
	return &WriteOffUseCaseReq{requestData: req, url: url}
}

func NewWriteOffUseCaseRes() *WriteOffUseCaseRes {
	return &WriteOffUseCaseRes{}
}
