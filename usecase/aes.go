package usecase

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"encoding/base64"
	"fmt"
)

func CBCEncrypt(plaintext, key, iv string) string {
	defer func() {
		if err := recover(); err != nil {
			fmt.Println("cbc decrypt err:", err)
		}
	}()

	block, err := aes.NewCipher([]byte(key))
	if err != nil {
		return err.Error()
	}

	blockSize := len(key)
	padding := blockSize - len(plaintext)%blockSize
	if padding == 0 {
		padding = blockSize
	}

	plaintext += string(bytes.Repeat([]byte{byte(padding)}, padding))
	ciphertext := []byte(plaintext)

	mode := cipher.NewCBCEncrypter(block, []byte(iv))
	mode.CryptBlocks(ciphertext, []byte(plaintext))

	return base64.StdEncoding.EncodeToString(ciphertext)
}

func CBCDecrypt(ciphertext, key, iv string) string {
	defer func() {
		if err := recover(); err != nil {
			fmt.Println("cbc decrypt err:", err)
		}
	}()

	block, err := aes.NewCipher([]byte(key))
	if err != nil {
		return err.Error()
	}

	ciphercode, err := base64.StdEncoding.DecodeString(ciphertext)
	if err != nil {
		return err.Error()
	}

	mode := cipher.NewCBCDecrypter(block, []byte(iv))
	mode.CryptBlocks(ciphercode, ciphercode)

	plaintext := string(ciphercode)
	return plaintext[:len(plaintext)-int(plaintext[len(plaintext)-1])]
}
