package query

import (
	"bytes"
	"ec-ticket/entity"
	"ec-ticket/usecase"
	"encoding/json"
	"net/http"
	"time"
)

type GetSingleProductUseCaseReq struct {
	requestData *entity.GetSingleProductRequest
	url         string
}

type GetSingleProductUseCaseRes struct {
	ResponseData *entity.GetSingleProductResponse
	Err          error
}

type GetSingleProductUseCase struct {
	req *GetSingleProductUseCaseReq
	res *GetSingleProductUseCaseRes
}

type singleProductData struct {
	MerchantID string `json:"MerchantID"`
	ItemNo     string `json:"ItemNo"`
}

func (gc *GetSingleProductUseCase) Execute() {
	// prepare data
	data := singleProductData{
		MerchantID: gc.req.requestData.MerchantID,
		ItemNo:     gc.req.requestData.ItemNo,
	}
	jsonData, err := json.Marshal(data)
	if err != nil {
		gc.res.Err = err
		return
	}
	encrypt := usecase.EncryptData(string(jsonData), gc.req.requestData.HashKey, gc.req.requestData.HashIV)
	checkValueEncode := usecase.EncryptCheckMacValue(string(jsonData), gc.req.requestData.HashKey, gc.req.requestData.HashIV)

	param, err := json.Marshal(&usecase.CommonParam{
		PlatformID:    gc.req.requestData.PlatformID,
		MerchantID:    gc.req.requestData.MerchantID,
		RqHeader:      usecase.RpHeader{Timestamp: time.Now().Add(time.Minute * 10).Unix()},
		Data:          encrypt,
		CheckMacValue: checkValueEncode,
	})
	if err != nil {
		gc.res.Err = err
		return
	}

	// send request
	response, err := http.Post(gc.req.url, "application/json", bytes.NewBuffer(param))
	if err != nil {
		gc.res.Err = err
		return
	}
	defer response.Body.Close()
	tmp := &usecase.CommonResponse{}
	err = json.NewDecoder(response.Body).Decode(tmp)
	if err != nil {
		gc.res.Err = err
		return
	}
	decodeData := usecase.DecryptData(tmp.Data, gc.req.requestData.HashKey, gc.req.requestData.HashIV)
	res := &entity.GetSingleProductResponse{}
	err = json.Unmarshal([]byte(decodeData), &res)
	if err != nil {
		gc.res.Err = err
		return
	}
	gc.res.ResponseData = res
}

func NewGetSingleProductUseCase(
	req *GetSingleProductUseCaseReq,
	res *GetSingleProductUseCaseRes,
) *GetSingleProductUseCase {
	return &GetSingleProductUseCase{
		req: req,
		res: res,
	}
}

func NewGetSingleProductUseCaseReq(
	requestData *entity.GetSingleProductRequest,
	url string,
) *GetSingleProductUseCaseReq {
	return &GetSingleProductUseCaseReq{
		requestData: requestData,
		url:         url,
	}
}

func NewGetSingleProductUseCaseRes() *GetSingleProductUseCaseRes {
	return &GetSingleProductUseCaseRes{}
}
