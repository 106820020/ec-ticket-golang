package usecase

import (
	"crypto/sha256"
	"encoding/hex"
	"net/url"
	"strings"
)

type CommonParam struct {
	PlatformID    string   `json:"PlatformID"`
	MerchantID    string   `json:"MerchantID"`
	RqHeader      RpHeader `json:"RqHeader"`
	Data          string   `json:"Data"`
	CheckMacValue string   `json:"CheckMacValue"`
}

type RpHeader struct {
	Timestamp int64 `json:"Timestamp"`
}

type CommonResponse struct {
	CommonParam
	TransCode int
	TransMsg  string
}

func EncryptData(jsonData, key, iv string) string {
	dataEncode := url.QueryEscape(jsonData)
	encrypt := CBCEncrypt(dataEncode, key, iv)
	return encrypt
}

func EncryptCheckMacValue(jsonData, key, iv string) string {
	dataEncode := strings.ToLower(url.QueryEscape(key + jsonData + iv))
	sha := sha256.Sum256([]byte(dataEncode))
	return strings.ToUpper(hex.EncodeToString(sha[:]))
}

func DecryptData(jsonData, key, iv string) string {
	decrypt := CBCDecrypt(jsonData, key, iv)
	dataDecode, err := url.QueryUnescape(decrypt)
	if err != nil {
		return err.Error()
	}
	return dataDecode
}
