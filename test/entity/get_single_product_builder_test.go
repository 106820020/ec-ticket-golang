package entity

import (
	"ec-ticket/entity"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestGetSingleProductRequest(t *testing.T) {
	req := entity.NewGetSingleProductRequestBuilder().
		WithMerchantID("3085676").
		WithPlatformID("3085676").
		HasHashIV("37a0ad3c6ffa428b").
		HasHashKey("7b53896b742849d3").
		WithItemNo("VJR57269").
		Build()
	assert.Equal(t, "3085676", req.MerchantID)
	assert.Equal(t, "3085676", req.PlatformID)
	assert.Equal(t, "37a0ad3c6ffa428b", req.HashIV)
	assert.Equal(t, "7b53896b742849d3", req.HashKey)
	assert.Equal(t, "VJR57269", req.ItemNo)
}
