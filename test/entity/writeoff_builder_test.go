package entity

import (
	"ec-ticket/entity"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestWriteOffRequest(t *testing.T) {
	req := entity.NewWriteOffRequestBuilder().
		WithPlatformID("1231231234").
		WithMerchantID("4564564567").
		HasHashKey("abc").
		HasHashIV("def").
		Action("1").
		WithWriteOffNo("AAA123").
		HasCancelReason("I want to use it right now").
		OperateBy("Winnie").
		Build()
	assert.Equal(t, "1231231234", req.PlatformID)
	assert.Equal(t, "4564564567", req.MerchantID)
	assert.Equal(t, "abc", req.HashKey)
	assert.Equal(t, "def", req.HashIV)
	assert.Equal(t, "1", req.Action)
	assert.Equal(t, "AAA123", req.WriteOffNo)
	assert.Equal(t, "I want to use it right now", req.CancelReason)
	assert.Equal(t, "Winnie", req.Operator)
}
