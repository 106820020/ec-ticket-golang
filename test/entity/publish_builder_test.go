package entity

import (
	"ec-ticket/entity"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestPublishRequestSingleBuild(t *testing.T) {
	req := entity.NewRequestBuilder().
		WithPlatformID("1231231234").
		WithMerchantID("4564564567").
		HasHashKey("abc").
		HasHashIV("def").
		WithCustomerName("Winnie").
		WithCustomerPhone("0912345678").
		WithCustomerEmail("abc@gmail.com").
		WithCustomerAddress("Taipei").
		WithMerchantTradeNo("AAA123").
		WithFreeTradeNo("BBB456").
		WithIssueType("3").
		WithPrintType("1").
		WithRefundNotifyURL("https://www.winnie.com").
		WithIsImmediate("1").
		OperateBy("Winnie").
		HasSingle(entity.NewTicketBuilder().WithItemNo("A0001").Has(2).StartFrom("20231012").Build()).
		Build()
	assert.Equal(t, "1231231234", req.PlatformID)
	assert.Equal(t, "4564564567", req.MerchantID)
	assert.Equal(t, "abc", req.HashKey)
	assert.Equal(t, "def", req.HashIV)
	assert.Equal(t, "Winnie", req.Customer.Name)
	assert.Equal(t, "0912345678", req.Customer.Phone)
	assert.Equal(t, "abc@gmail.com", req.Customer.Email)
	assert.Equal(t, "Taipei", req.Customer.Address)
	assert.Equal(t, "AAA123", req.MerchantTradeNo)
	assert.Equal(t, "BBB456", req.FreeTradeNo)
	assert.Equal(t, "3", req.IssueType)
	assert.Equal(t, "1", req.PrintType)
	assert.Equal(t, "https://www.winnie.com", req.RefundNotifyURL)
	assert.Equal(t, "1", req.IsImmediate)
	assert.Equal(t, "Winnie", req.Operator)
	assert.Equal(t, 1, len(req.PublishData.TicketInfo))
	assert.Equal(t, "A0001", req.PublishData.TicketInfo[0].ItemNo)
	assert.Equal(t, 2, req.PublishData.TicketInfo[0].TicketAmount)
}

func TestPublishRequestMultipleBuild(t *testing.T) {
	ticket1 := entity.NewTicketBuilder().WithItemNo("A0001").Has(2).StartFrom("20231012").Build()
	ticket2 := entity.NewTicketBuilder().WithItemNo("A0002").Has(3).StartFrom("20231012").Build()
	req := entity.NewRequestBuilder().
		WithPlatformID("1231231234").
		WithMerchantID("4564564567").
		HasHashKey("abc").
		HasHashIV("def").
		WithCustomerName("Winnie").
		WithCustomerPhone("0912345678").
		WithCustomerEmail("abc@gmail.com").
		WithCustomerAddress("Taipei").
		WithMerchantTradeNo("AAA123").
		WithFreeTradeNo("BBB456").
		WithIssueType("3").
		WithPrintType("1").
		WithRefundNotifyURL("https://www.winnie.com").
		WithIsImmediate("1").
		OperateBy("Winnie").
		HasMultiple([]*entity.Ticket{ticket1, ticket2}).
		Build()

	assert.Equal(t, "1231231234", req.PlatformID)
	assert.Equal(t, "4564564567", req.MerchantID)
	assert.Equal(t, "abc", req.HashKey)
	assert.Equal(t, "def", req.HashIV)
	assert.Equal(t, "Winnie", req.Customer.Name)
	assert.Equal(t, "0912345678", req.Customer.Phone)
	assert.Equal(t, "abc@gmail.com", req.Customer.Email)
	assert.Equal(t, "Taipei", req.Customer.Address)
	assert.Equal(t, "AAA123", req.MerchantTradeNo)
	assert.Equal(t, "BBB456", req.FreeTradeNo)
	assert.Equal(t, "3", req.IssueType)
	assert.Equal(t, "1", req.PrintType)
	assert.Equal(t, "https://www.winnie.com", req.RefundNotifyURL)
	assert.Equal(t, "1", req.IsImmediate)
	assert.Equal(t, "Winnie", req.Operator)
	assert.Equal(t, 2, len(req.PublishData.TicketInfo))
	assert.Equal(t, "A0001", req.PublishData.TicketInfo[0].ItemNo)
	assert.Equal(t, 2, req.PublishData.TicketInfo[0].TicketAmount)
	assert.Equal(t, "A0002", req.PublishData.TicketInfo[1].ItemNo)
	assert.Equal(t, 3, req.PublishData.TicketInfo[1].TicketAmount)
}
