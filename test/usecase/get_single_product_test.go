package usecase

import (
	"ec-ticket/entity"
	"ec-ticket/usecase/query"
	"fmt"
	"testing"
)

func TestGetSingleProductUseCase_Execute(t *testing.T) {
	data := entity.NewGetSingleProductRequestBuilder().
		WithMerchantID("3085676").
		WithPlatformID("3085676").
		HasHashIV("37a0ad3c6ffa428b").
		HasHashKey("7b53896b742849d3").
		WithItemNo("VJR57269").
		Build()
	req := query.NewGetSingleProductUseCaseReq(&data, "https://ecticket-stage.ecpay.com.tw/api/Ticket/QueryItemInfo")
	res := query.NewGetSingleProductUseCaseRes()
	uc := query.NewGetSingleProductUseCase(req, res)
	uc.Execute()

	if res.Err != nil {
		t.Error("failed to execute usecase", res.Err)
	}

	fmt.Println(res.ResponseData)
}
