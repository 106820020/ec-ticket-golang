package usecase

import (
	"ec-ticket/entity"
	"ec-ticket/usecase/command"
	"fmt"
	"testing"
)

func TestWriteOffUseCase_Execute_WriteOff(t *testing.T) {
	data := entity.NewWriteOffRequestBuilder().
		WithMerchantID("3085676").
		HasHashKey("7b53896b742849d3").
		HasHashIV("37a0ad3c6ffa428b").
		Action("1").
		WithWriteOffNo("RA8NCNVNGVY6NHUY4G").
		OperateBy("Winnie").
		Build()
	req := command.NewWriteOffUseCaseReq(&data, "https://ecticket-stage.ecpay.com.tw/api/Ticket/WriteOff")
	res := command.NewWriteOffUseCaseRes()
	uc := command.NewWriteOffUseCase(req, res)
	uc.Execute()

	if res.Err != nil {
		t.Error("failed to execute usecase", res.Err)
	}
	fmt.Println(res.ResponseData)
}

func TestWriteOffUseCase_Execute_Cancel(t *testing.T) {
	data := entity.NewWriteOffRequestBuilder().
		WithMerchantID("3085676").
		HasHashKey("7b53896b742849d3").
		HasHashIV("37a0ad3c6ffa428b").
		Action("2").
		WithWriteOffNo("RA8NCNVNGVY6NHUY4G").
		OperateBy("Winnie").
		Build()
	req := command.NewWriteOffUseCaseReq(&data, "https://ecticket-stage.ecpay.com.tw/api/Ticket/WriteOff")
	res := command.NewWriteOffUseCaseRes()
	uc := command.NewWriteOffUseCase(req, res)
	uc.Execute()

	if res.Err != nil {
		t.Error("failed to execute usecase", res.Err)
	}
	fmt.Println(res.ResponseData)
}
