package usecase

import (
	"ec-ticket/entity"
	"ec-ticket/usecase/command"
	"fmt"
	"testing"
)

func TestPublishTicketUseCase_Execute(t *testing.T) {
	data := entity.NewRequestBuilder().
		WithMerchantID("3085676").
		HasHashKey("7b53896b742849d3").
		HasHashIV("37a0ad3c6ffa428b").
		WithCustomerName("Winnie").
		WithCustomerPhone("0912345678").
		WithCustomerEmail("abc@gmail.com").
		WithCustomerAddress("Taipei").
		WithMerchantTradeNo("foenugelfjdsifhg").
		WithFreeTradeNo("").
		WithIssueType("2").
		WithPrintType("1").
		WithRefundNotifyURL("http://ecticket.ecpay.com.tw/RefundNotify").
		OperateBy("Winnie").
		HasSingle(entity.NewTicketBuilder().WithItemNo("VJR57269").Has(2).Build()).
		Build()
	req := command.NewPublishTicketUseCaseReq(&data, "https://ecticket-stage.ecpay.com.tw/api/Ticket/Issue")
	res := command.NewPublishTicketUseCaseRes()
	uc := command.NewPublishTicketUseCase(req, res)
	uc.Execute()

	if res.Err != nil {
		t.Error("failed to execute usecase", res.Err)
	}
	fmt.Println(res.ResponseData)
}
